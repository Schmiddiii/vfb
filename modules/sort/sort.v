module sort

import files { FileDescription }
import colors

pub enum Sort {
	alphabetic
	modified
}

pub fn (s Sort) cmp(f1 &FileDescription, f2 &FileDescription) int {
	match s {
		.alphabetic {
			f1_lower := f1.name.to_lower()
			f2_lower := f2.name.to_lower()
			if f1_lower < f2_lower {
				return -1
			} else {
				return 1
			}
		}
		.modified {
			if f1.last_modified < f2.last_modified {
				return 1
			} else {
				return -1
			}
		}
	}
}

pub fn (s Sort) str() string {
	mut str := ''
	if s == .alphabetic {
		str += colors.sort_show.apply('A')
	} else {
		str += 'A'
	}
	if s == .modified {
		str += colors.sort_show.apply('M')
	} else {
		str += 'M'
	}
return str
}
