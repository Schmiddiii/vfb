module show

import colors

[flag]
pub enum Show {
	dir
	file
	link
	hidden
	ignored
}

pub fn (s Show) str() string {
	mut str := ''
	if s.has(.dir) {
		str += colors.options_show.apply('D')
	} else {
		str += 'D'
	}
	if s.has(.file) {
		str += colors.options_show.apply('F')
	} else {
		str += 'F'
	}
	if s.has(.link) {
		str += colors.options_show.apply('L')
	} else {
		str += 'L'
	}
	if s.has(.hidden) {
		str += colors.options_show.apply('H')
	} else {
		str += 'H'
	}
	if s.has(.ignored) {
		str += colors.options_show.apply('I')
	} else {
		str += 'I'
	}
	return str
}
