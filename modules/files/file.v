module files

import os
import time
import colors

pub struct FileDescription {
pub:
	name          string
	fullpath      string
	last_modified i64
	inode         os.FileMode
	typ           FileType
}

pub fn (f FileDescription) last_modified_str() string {
	time := time.unix(f.last_modified + time.offset())
// TODO: Config
	return time.custom_format("YY-MM-dd HH:mm")
}

fn perm_str(f os.FilePermission) string {
	mut str := ''
	if f.read {
		str += 'r'
	} else {
		str += '-'
	}
	if f.write {
		str += 'w'
	} else {
		str += '-'
	}
	if f.execute {
		str += 'x'
	} else {
		str += '-'
	}
	return str
}

pub fn (f FileDescription) inode_str() string {
	return perm_str(f.inode.owner) + perm_str(f.inode.group) + perm_str(f.inode.others)
}

pub fn (f FileDescription) str() string {
	return f.name
}

pub enum FileType {
	dir
	file
	link
}

pub fn path_to_type(path string) FileType {
	if os.is_dir(path) {
		return FileType.dir
	} else if os.is_file(path) {
		return FileType.file
	} else {
		return FileType.link
	}
}

pub fn type_col(t FileType, s string) string {
	match t {
		.dir { return colors.dir.apply(s) }
		.file { return colors.file.apply(s) }
		.link { return colors.link.apply(s) }
	}
}

pub fn type_col_hl(t FileType, s string) string {
	match t {
		.dir { return colors.dir_highlight.apply(s) }
		.file { return colors.file_highlight.apply(s) }
		.link { return colors.link_highlight.apply(s) }
	}
}
