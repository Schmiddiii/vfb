module colors

import colors { Color, Colors }

// Change the colors of the application.
// Each value has the form:
//
// ```
// Colors {
// fg: Color.x
// bg: Color.y
// }
// ```
// You can also leave out `fg` or `bg` to make it transparent.
// The available `Color`-values are (standard ANSI 16-color):
// - black
// - red
// - green
// - yellow
// - blue
// - purple
// - cyan
// - white
// - bright_black
// - bright_red
// - bright_green
// - bright_yellow
// - bright_blue
// - bright_purple
// - bright_cyan
// - bright_white

pub const (
	// The color of the path at the top.
	path = Colors{
		fg: Color.green
	}
	// The color of the filter.
	filter = Colors{
		fg: Color.cyan
	}
	// Color of un-highlighted file.
	file           = Colors{}
	// Color of highlighted file.
	file_highlight = Colors{
		fg: Color.black
		bg: Color.white
	}
	// Color of un-highlighted folder.
	dir = Colors{
		fg: Color.purple
	}
	// Color of highlighted folder.
	dir_highlight = Colors{
		fg: Color.black
		bg: Color.purple
	}
	// Color of un-highlighted link.
	link = Colors{
		fg: Color.blue
	}
	// Color of highlighted link.
	link_highlight = Colors{
		fg: Color.black
		bg: Color.blue
	}
	// Color of option when turned on.
	options_show = Colors{
		fg: Color.green
	}
	// Color of runtimeconfig when turned on.
	runtimeconfig_show = Colors{
		fg: Color.green
	}
	// Color of sorting when turned on.
	sort_show = Colors{
		fg: Color.green
	}
)
