module colors

import term

pub struct Colors {
	fg Color
	bg Color
}

pub fn (c Colors) apply(s string) string {
	return c.fg.fg(c.bg.bg(s))
}

pub enum Color {
	@none
	black
	red
	green
	yellow
	blue
	purple
	cyan
	white
	bright_black
	bright_red
	bright_green
	bright_yellow
	bright_blue
	bright_purple
	bright_cyan
	bright_white
}

pub fn (c Color) fg(s string) string {
	match c {
		.@none { return s }
		.black { return term.black(s) }
		.red { return term.red(s) }
		.green { return term.green(s) }
		.yellow { return term.yellow(s) }
		.blue { return term.blue(s) }
		.purple { return term.magenta(s) }
		.cyan { return term.cyan(s) }
		.white { return term.gray(s) }
		.bright_black { return term.bright_black(s) }
		.bright_red { return term.bright_red(s) }
		.bright_green { return term.bright_green(s) }
		.bright_yellow { return term.bright_yellow(s) }
		.bright_blue { return term.bright_blue(s) }
		.bright_purple { return term.bright_magenta(s) }
		.bright_cyan { return term.bright_cyan(s) }
		.bright_white { return term.white(s) }
	}
}

pub fn (c Color) bg(s string) string {
	match c {
		.@none { return s }
		.black { return term.bg_black(s) }
		.red { return term.bg_red(s) }
		.green { return term.bg_green(s) }
		.yellow { return term.bg_yellow(s) }
		.blue { return term.bg_blue(s) }
		.purple { return term.bg_magenta(s) }
		.cyan { return term.bg_cyan(s) }
		.white { return term.bg_white(s) }
		.bright_black { return term.bright_bg_black(s) }
		.bright_red { return term.bright_bg_red(s) }
		.bright_green { return term.bright_bg_green(s) }
		.bright_yellow { return term.bright_bg_yellow(s) }
		.bright_blue { return term.bright_bg_blue(s) }
		.bright_purple { return term.bright_bg_magenta(s) }
		.bright_cyan { return term.bright_bg_cyan(s) }
		.bright_white { return term.bright_bg_white(s) }
	}
}
