module listing

import show { Show }
import sort { Sort }
import files { FileDescription }
import config
import log
import os

pub struct Listing {
pub:
	path string [required]
mut:
	files []FileDescription
pub mut:
	filter string
	show   Show = config.show
	sort   Sort = config.sort
}

pub fn (mut l Listing) list(mut log log.Log) []FileDescription {
	if l.files.len == 0 {
		strings := os.ls(l.path) or {
			log.error('Failed to list files for $l.path')
			['Error']
		}
		l.files = strings.map(FileDescription{
			name: it
			fullpath: os.join_path_single(l.path, it)
			inode: os.inode(os.join_path_single(l.path, it))
			last_modified: os.file_last_mod_unix(os.join_path_single(l.path, it))
			typ: files.path_to_type(os.join_path_single(l.path, it))
		})
		log.debug('Populated file listing for $l.path: $l.files')
	}
	mut filtered := l.files
		.filter(if config.ignorecase {
			it.name.to_lower().contains(l.filter.to_lower())
		} else {
			it.name.contains(l.filter)
		})
		.filter(l.show.has(.file) || it.typ != .file)
		.filter(l.show.has(.dir) || it.typ != .dir)
		.filter(l.show.has(.link) || it.typ != .link)
		.filter(l.show.has(.hidden) || !it.name.starts_with('.'))
		.filter(l.show.has(.ignored)
			|| os.execute('cd $l.path;
			git check-ignore $it.name').exit_code != 0)
        filtered.sort_with_compare(l.sort.cmp)
	return filtered
}

pub fn (lst Listing) cd(dir string) Listing {
	return Listing{
		path: lst.path + '/' + dir
	}
}

pub fn (lst Listing) parent() Listing {
	return Listing{
		path: lst.path.all_before_last('/')
	}
}

