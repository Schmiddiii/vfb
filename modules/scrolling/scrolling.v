module scrolling

pub struct Scrolling {
pub:
	edge_scroll int = 5
	elem_size   int [required]
	win_size    int [required]
mut:
	cursor int
	offset int
}

pub fn (s Scrolling) cursor() int {
	return s.cursor
}

pub fn (s Scrolling) offset() int {
	return s.offset
}

pub fn (s Scrolling) position(elem int) int {
	if elem < s.offset || elem >= s.offset + s.win_size {
		return -1
	} else {
		return elem - s.offset
	}
}

pub fn (mut s Scrolling) up() {
	s.cursor = (s.cursor - 1 + s.elem_size) % s.elem_size
	s.offset = s.cursor - (s.cursor % s.win_size)
}

pub fn (mut s Scrolling) down() {
	s.cursor = (s.cursor + 1) % s.elem_size
	s.offset = s.cursor - (s.cursor % s.win_size)
}
