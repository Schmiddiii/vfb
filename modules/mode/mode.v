module mode

pub enum Mode {
	filter
	normal
	options
	config
	sort
}

pub fn (m Mode) str() string {
	match m {
		.filter { return 'FILTER' }
		.normal { return 'NORMAL' }
		.options { return 'OPTIONS' }
		.config { return 'CONFIG' }
		.sort { return 'SORT' }
	}
}
