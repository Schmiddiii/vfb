module config

import os

// Specification on how to open what files.
// The opening is dependent on the file extension.

const (
	// When nothing can be found, execute this.
	default = Command{
		executable: 'nvim'
		block: true
	}
	// How to open what file types. Each instruction on how to open file types is a `Command` with a executable (the command to run) and whether to block (blocking should only be done when opening tui-applications).
	open_map = {
		'pdf': Command{
			executable: 'zathura'
			block: false
		}
	}
)

pub struct Command {
pub:
	executable string
	block      bool
}

pub fn open_command(filename string) Command {
	extension_with_leading_dot := os.file_ext(filename)
	extension := extension_with_leading_dot.trim_left('.')
	return config.open_map[extension] or { config.default }
}
