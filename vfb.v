import term.ui as tui
import log
import os
import listing { Listing }
import files { type_col, type_col_hl }
import mode { Mode }
import scrolling { Scrolling }
import runtimeconfig { Runtimeconfig }
import config
import colors
import math

const edge_scroll = 5

const non_list_len = 5

const list_offset_top = 3

const listing_barrier = 2

struct App {
mut:
	tui           &tui.Context = 0
	l             log.Log      = log.Log{}
	listing       Listing
	mode          Mode = config.default_mode
	runtimeconfig Runtimeconfig = config.default_runtimeconfig
	scroll        Scrolling
}

fn (mut a App) cd() {
	element := a.listing.list(mut a.l)[a.scroll.cursor()]
	path := element.str()
	if element.typ == .file {
		a.l.debug('Open $element.fullpath')
		command := config.open_command(path)
		mut process := os.new_process(os.find_abs_path_of_executable(command.executable) or {
			a.l.error('Could not find program to open file with')
			return
		})
		process.set_args(['$element.fullpath'])
		a.l.debug('Process: $process')
		process.run()
		if command.block {
			process.wait()
		}
	} else {
		a.listing = a.listing.cd(path)
		a.l.debug('Set path to $path')
		a.scroll = Scrolling{
			win_size: a.tui.window_height - non_list_len
			elem_size: a.listing.list(mut a.l).len
		}
	}
}

fn event(e &tui.Event, x voidptr) {
	mut a := &App(x)
	a.l.debug(e.str())
	if a.mode == .filter && e.typ == .key_down {
		if e.code == .backspace {
			if a.listing.filter.len != 0 {
				a.listing.filter = a.listing.filter.limit(a.listing.filter.len - 1)
			} else {
				a.listing = a.listing.parent()
				a.l.debug('Set path to $a.listing.path')
			}
		} else if e.code == .n && e.modifiers.has(.ctrl) {
			a.scroll.down()
		} else if e.code == .p && e.modifiers.has(.ctrl) {
			a.scroll.up()
		} else if e.code == .escape {
			a.mode = .normal
		} else if e.code == .enter {
			a.cd()
		} else if (int(e.code) >= 48 && int(e.code) <= 57)
			|| (int(e.code) >= 97 && int(e.code) <= 122) {
			a.listing.filter += e.ascii.ascii_str()
			a.l.debug('Set filter to $a.listing.filter')
			if a.listing.list(mut a.l).len == 1 && config.autocd {
				a.cd()
			}
			a.scroll = Scrolling{
				win_size: a.tui.window_height - non_list_len
				elem_size: a.listing.list(mut a.l).len
			}
		}
	} else if a.mode == .normal && e.typ == .key_down {
		if e.code == .q {
			exit(0)
		} else if e.code == .j {
			a.scroll.down()
		} else if e.code == .k {
			a.scroll.up()
		} else if e.code == .f {
			a.mode = .filter
		} else if e.code == .o {
			a.mode = .options
		} else if e.code == .c {
			a.mode = .config
		} else if e.code == .s {
			a.mode = .sort
		} else if e.code == .enter {
			a.cd()
		} else if e.code == .backspace {
			a.listing = a.listing.parent()
		}
	} else if a.mode == .options && e.typ == .key_down {
		if e.code == .f {
			a.listing.show.toggle(.file)
		} else if e.code == .d {
			a.listing.show.toggle(.dir)
		} else if e.code == .l {
			a.listing.show.toggle(.link)
		} else if e.code == .h {
			a.listing.show.toggle(.hidden)
		} else if e.code == .i {
			a.listing.show.toggle(.ignored)
		} else if e.code == .escape {
			a.mode = .normal
		}
	} else if a.mode == .config && e.typ == .key_down {
		if e.code == .m {
			a.runtimeconfig.toggle(.show_modified)
		} else if e.code == .p {
			a.runtimeconfig.toggle(.show_permissions)
		} else if e.code == .escape {
			a.mode = .normal
		}
	} else if a.mode == .sort && e.typ == .key_down {
		if e.code == .a {
			a.l.debug('Change sort to alphabetic')
			a.listing.sort = .alphabetic
		} else if e.code == .m {
			a.l.debug('Change sort to modified')
			a.listing.sort = .modified
		} else if e.code == .escape {
			a.mode = .normal
		}
	}
	frame(x)
}

fn frame(x2 voidptr) {
	mut app := &App(x2)

	app.tui.clear()
	app.tui.draw_text(0, 1, colors.path.apply(app.listing.path))
	app.tui.draw_text(0, 2, colors.filter.apply(app.listing.filter))
	mut max_len_file := 0
	for i, file in app.listing.list(mut app.l) {
		rel := app.scroll.position(i)
		if rel < 0 {
			continue
		}
		max_len_file = math.max(max_len_file, file.str().len)

		if i == app.scroll.cursor() {
			app.tui.draw_text(0, rel + list_offset_top, type_col_hl(file.typ, file.str()))
		} else {
			app.tui.draw_text(0, rel + list_offset_top, type_col(file.typ, file.str()))
		}
	}
	mut max_len_modified := -listing_barrier
	if app.runtimeconfig.has(.show_modified) {
		for i, file in app.listing.list(mut app.l) {
			rel := app.scroll.position(i)
			if rel < 0 {
				continue
			}
			modified := file.last_modified_str()
			max_len_modified = math.max(max_len_modified, modified.len)
			app.tui.draw_text(max_len_file + listing_barrier, rel + list_offset_top, modified)
		}
	}
	mut max_len_perm := -listing_barrier
	if app.runtimeconfig.has(.show_permissions) {
		for i, file in app.listing.list(mut app.l) {
			rel := app.scroll.position(i)
			if rel < 0 {
				continue
			}
			permission := file.inode_str()
			max_len_perm = math.max(max_len_perm, permission.len)
			app.tui.draw_text(max_len_file + max_len_modified + 2 * listing_barrier, rel +
				list_offset_top, permission)
		}
	}
	app.tui.draw_text(0, app.tui.window_height - 1, app.mode.str())
	app.tui.draw_text(app.tui.window_width - 7, app.tui.window_height - 1, app.listing.show.str())
	app.tui.draw_text(app.tui.window_width - 7, app.tui.window_height - 2, app.runtimeconfig.str())
	app.tui.draw_text(app.tui.window_width - 7, app.tui.window_height - 3, app.listing.sort.str())
	app.tui.set_cursor_position(0, 0)

	app.tui.reset()
	app.tui.flush()
}

fn cleanup(x3 voidptr) {
	mut app2 := &App(x3)
	app2.l.flush()
}

fn init(x4 voidptr) {
	mut a2 := &App(x4)
	a2.scroll = Scrolling{
		win_size: a2.tui.window_height - non_list_len
		elem_size: a2.listing.list(mut a2.l).len
	}
	frame(x4)
}

fn main() {
	mut l := log.Log{}
	$if debug {
		l.set_level(.debug)
		l.set_full_logpath('./vfb.log')
		l.info('Start Logging')
	}
	mut path := ''
	if os.args.len > 1 {
		path = os.args[1]
	}
	mut app3 := &App{
		l: l
		listing: Listing{
			path: os.abs_path(path)
		}
	}
	app3.tui = tui.init(
		user_data: app3
		event_fn: event
		init_fn: init
		cleanup_fn: cleanup
		hide_cursor: true
		window_title: 'vfb'
	)
	app3.tui.run()?
}
