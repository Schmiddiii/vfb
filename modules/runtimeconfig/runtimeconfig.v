module runtimeconfig

import colors

[flag]
pub enum Runtimeconfig {
	show_modified
	show_permissions
}

pub fn (r Runtimeconfig) str() string {
	mut str := ''
	if r.has(.show_modified) {
		str += colors.runtimeconfig_show.apply('M')
	} else {
		str += 'M'
	}
	if r.has(.show_permissions) {
		str += colors.runtimeconfig_show.apply('P')
	} else {
		str += 'P'
	}
	return str
}
