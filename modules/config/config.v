module config

import mode { Mode }
import show { Show }
import sort { Sort }
import runtimeconfig { Runtimeconfig }

pub const (
	// Auto-cd into a directory when filtering and only one result is left.
	autocd                = true
	// Ignore the case when filtering.
	ignorecase            = true
	// The default mode when starting the application.
	// Possible values:
	// - Mode.normal
	// - Mode.filter
	// - Mode.options
	// - Mode.config
	default_mode          = Mode.normal
	// What options to show by default.
	// Possible values:
	// - Show.file
	// - Show.dir
	// - Show.link
	// - Show.hidden
	// - Show.ignored
	// Multiple options can be set by appending with `|`.
	show                  = Show.file | Show.dir | Show.link | Show.hidden | Show.ignored
	// How to sort by default.
	// Possible values:
	// - Sort.alphabetic
	// - Sort.modified
	sort                  = Sort.alphabetic
	// Default runtime configuration
	// Possible values:
	// - Runtimeconfig.show_modified
	// - Runtimeconfig.show_permissions
	// Multiple options can be set by appending with `|`.
	default_runtimeconfig = Runtimeconfig.show_modified | Runtimeconfig.show_permissions
	// How to format date-times.
	// See https://modules.vlang.io/time.html#Time.custom_format.
	format_datetime       = 'YYYY-MM-DD HH:mm'
)
