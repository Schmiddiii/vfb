# vfb

V file browser

## Introduction

This is a very quick and simple file browser written in [V](https://vlang.io).
Note the difference between file manager and file browser, this application is not (yet?) intended to manage (copy, move, rename, ...) your files, it only represents a way to browse through your files.

While only being able to browse through files, it is very good at that, including many features useful for that.
This should also showcase a few ideas that might be useful for file-managers.

## Features

### Modal

This file browser is modal, meaning it has three different modes where different buttons mean different things.
Current modes include:

- Normal: Normally scroll through your files (using `j` and `k`) or enter folders (`enter`)
- Filter: Filter all shown files by name. 
- Options: Filter all shown files by attributes
- Config: Modify the run-time configuration (e.g. what attributes to show for files)
- Sort: Modify how to sort the files.

### Filter

This mode can be reached by pressing `f` in the normal mode. 
You can then type to reduce the folder listing to just include files by name (currently only case-sensitive), scroll through the files using `Ctrl-n` and `Ctrl-p` and enter folders (`enter`).
When only one folder entry is left, this entry will be entered automatically.
You may leave the mode using `escape`

### Options

This mode can be reached by pressing `o` in the normal mode. 
You can then choose what files are hidden by their attributes (file, directory, link, hidden, git-ignored) by pressing a letter (`f` for file, `d` for directory, `l` for link, `h` for hidden and `i` for ignored).
The current status of the options can be found in the bottom-right corner.
You may leave the mode using `escape`.

### Config

This mode can be reached by pressing `c` in the normal mode. 
You can then choose which attributes to show by pressing the letter `m` for last modified date and `p` for permissions.
The current status of the run-time configuration can be found in the bottom-right corner.
You may leave the mode using `escape`.

### Sort

This mode can be reached by pressing `s` in the normal mode. 
You can then choose how to sort by pressing the letter `a` for last alphabetical and `m` for modified.
The current status of the sorting can be found in the bottom-right corner.
You may leave the mode using `escape`.

## Usage

Install [V](https://vlang.io).
The version has to be extremely new (at the time of writing, the git-version is required).
Compile the source by `v -prod vfb.v`, the resulting binary `vfb` should be put in the `$PATH`.

Execute the program with `vfb` with a optional path (the current directory by default).

## Configuration

Change the values in `modules/config/config.v` and recompile the program.
The documentation of the configuration options is already in the configuration file.

To change the colors, edit `modules/color/config.v`.

## Future Ideas

- File/folder preview
- Customization file (no auto-enter, preset options, maybe keys)
- Full file manager
